<?php

namespace WA\UtilisateurBundle\Entity;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class Utilisateur extends BaseUser
{
    /** 
     *@var integer
     */
    protected $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $notes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $conferencesCrees;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $conferencesSuivies;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commentaires;


    private $imageProfil;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->commentaires = new \Doctrine\Common\Collections\ArrayCollection();
        $this->firstlogin = new \DateTime();
    }

    /**
     * Add commentaire
     *
     * @param \WA\CodenconfBundle\Entity\Commentaire $commentaire
     *
     * @return Utilisateur
     */
    public function addCommentaire(\WA\CodenconfBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    /**
     * Remove commentaire
     *
     * @param \WA\CodenconfBundle\Entity\Commentaire $commentaire
     */
    public function removeCommentaire(\WA\CodenconfBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires->removeElement($commentaire);

    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Get count commentaires
     *
     * @return int
     */
    public function getCountCommentaires()
    {
        return count($this->commentaires);
    }

    /**
     * Add note
     *
     * @param \WA\CodenconfBundle\Entity\Note $note
     *
     * @return Utilisateur
     */
    public function addNote(\WA\CodenconfBundle\Entity\Note $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \WA\CodenconfBundle\Entity\Note $note
     */
    public function removeNote(\WA\CodenconfBundle\Entity\Note $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add conferencesCree
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conferencesCree
     *
     * @return Utilisateur
     */
    public function addConferencesCree(\WA\CodenconfBundle\Entity\Conference $conferencesCree)
    {
        $this->conferencesCrees[] = $conferencesCree;

        return $this;
    }

    /**
     * Remove conferencesCree
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conferencesCree
     */
    public function removeConferencesCree(\WA\CodenconfBundle\Entity\Conference $conferencesCree)
    {
        $this->conferencesCrees->removeElement($conferencesCree);
    }

    /**
     * Get conferencesCrees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConferencesCrees()
    {
        return $this->conferencesCrees;
    }

    /**
     * Get count conferencesCrees
     *
     * @return int
     */
    public function getCountConferencesCrees()
    {
        return count($this->conferencesCrees);
    }

    /**
     * Add conferencesSuivy
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conferencesSuivy
     *
     * @return Utilisateur
     */
    public function addConferencesSuivies(\WA\CodenconfBundle\Entity\Conference $conferencesSuivies)
    {
        $this->conferencesSuivies[] = $conferencesSuivies;

        return $this;
    }

    /**
     * Remove conferencesSuivy
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conferencesSuivy
     */
    public function removeConferencesSuivies(\WA\CodenconfBundle\Entity\Conference $conferencesSuivies)
    {
        $this->conferencesSuivies->removeElement($conferencesSuivies);
    }

    /**
     * Get conferencesSuivies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConferencesSuivies()
    {
        return $this->conferencesSuivies;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Utilisateur
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Utilisateur
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    
    /**
     * @var \DateTime
     */
    private $firstlogin;


    /**
     * Set firstlogin
     *
     * @param \DateTime $firstlogin
     *
     * @return Utilisateur
     */
    public function setFirstlogin($firstlogin)
    {
        $this->firstlogin = $firstlogin;

        return $this;
    }

    /**
     * Get firstlogin
     *
     * @return \DateTime
     */
    public function getFirstlogin()
    {
        return $this->firstlogin;
    }

    /**
     * Set imageProfil
     *
     * @param \WA\CodenconfBundle\Entity\Image $imageProfil
     *
     * @return Utilisateur
     */
    public function setImageProfil(\WA\CodenconfBundle\Entity\Image $imageProfil = null)
    {
        $this->imageProfil = $imageProfil;

        return $this;
    }

     /**
     * Get imagePresentation
     *
     * @return \WA\CodenconfBundle\Entity\Image
     */
    public function getImageProfil()
    {
        return $this->imageProfil;
    }
}
