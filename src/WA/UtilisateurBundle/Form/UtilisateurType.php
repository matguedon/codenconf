<?php

namespace WA\UtilisateurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UtilisateurType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('firstlogin')
            ->add('imageProfil')
            ->add('conferencesSuivies')
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wa_utilisateurbundle_utilisateur';
    }
}
