<?php

namespace WA\UtilisateurBundle\Form;

use WA\CodenconfBundle\Form\NewImageType;
use WA\UtilisateurBundle\Form\UtilisateurType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewUtilisateurType extends UtilisateurType
{
	 /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	parent::buildForm($builder, $options);
        $builder
            ->remove('firstlogin')
            ->remove('conferencesSuivies')
            ->add('imageProfil', new NewImageType())
            ->add('save', 'submit', array('label' => 'Enregistrer'))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wa_utilisateurbundle_new_utilisateur_type';
    }
}