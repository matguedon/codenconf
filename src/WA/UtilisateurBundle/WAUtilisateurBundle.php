<?php

namespace WA\UtilisateurBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class WAUtilisateurBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
