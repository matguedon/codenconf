<?php

namespace WA\UtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Request;
use WA\UtilisateurBundle\Entity\Utilisateur as User;
use FOS\UserBundle\Doctrine\UserManager;
use WA\CodenconfBundle\Form\NewImageType;
use WA\UtilisateurBundle\Form\UtilisateurType;
use WA\UtilisateurBundle\Form\EditUtilisateurType;



class DefaultController extends Controller
{
	public function indexAction($name)
	{
		return $this->render('WAUtilisateurBundle:Default:index.html.twig', array('name' => $name));
	}

	public function monCompteAction(Request $request)
	{
		$em = $this->get('doctrine')->getManager();

		$utilisateurCourant = $this->container->get('security.context')->getToken()->getUser();
		$utilisateur = $em->getRepository('WAUtilisateurBundle:Utilisateur')
		->findById($utilisateurCourant)[0];

		$password = $utilisateur->getPassword();
		$utilisateur->setPlainPassword($password);

		$form = $this->createForm(new EditUtilisateurType(), $utilisateur);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$imageProfil = $utilisateur->getImageProfil();
			$titre = "photo_profil_" . $utilisateur->getUsername();
			$imageProfil->setName($titre);
			$imageProfil->upload($titre);

			$em->persist($imageProfil);
			$em->flush();

    		//	$request->getSession()->getFlashBag()->add('notice', 'Conférence bien enregistrée.');

			return $this->redirectToRoute('wa_codenconf_accueil');
		}

		$breadcrumb = array();
		array_push($breadcrumb, "Mon compte");

		return $this->render('WAUtilisateurBundle:Default:monCompte.html.twig', array(
			'form' => $form->createView(),
			'breadcrumb' => $breadcrumb,
			'utilisateur' => $utilisateur
			));
	}

	public function profilAction($id)
	{
		$em = $this->get('doctrine')->getManager();
		$utilisateur = $em->getRepository('WAUtilisateurBundle:Utilisateur')
		->findById($id);

		$breadcrumb = array();
		array_push($breadcrumb, "Mon profil");

		return $this->render('WAUtilisateurBundle:Default:profil.html.twig', array(
			'breadcrumb' => $breadcrumb,
			'utilisateur' => $utilisateur[0]
			));
	}

	public function deleteUserAction(){
		$user = $this->container->get('security.context')->getToken()->getUser();
		$userManager = $this->container->get('fos_user.user_manager');
		$userManager->deleteUser($user);
		$this->get('session')->getFlashBag()->add('success', $user->getUsername() . 'Utilisateur supprimé');
		return $this->redirect($this->generateUrl('wa_codenconf_accueil'));

	}

	public function administrationAction()
	{
		$breadcrumb = array();
		array_push($breadcrumb, "Administration");

		$em = $this->get('doctrine')->getManager();
		$utilisateurs = $em->getRepository('WAUtilisateurBundle:Utilisateur')
		->findAll();



		$conferences = $em->getRepository('WACodenconfBundle:Conference')
		->findAll();



		arsort($utilisateurs);
		arsort($utilisateurs);
		return $this->render('WAUtilisateurBundle:Default:administration.html.twig', array(
			'breadcrumb' => $breadcrumb,
			'conferences' => $conferences,
			'utilisateurs' => $utilisateurs

			));

	}

}
