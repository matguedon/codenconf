<?php

namespace WA\CodenconfBundle\Entity;

/**
 * Categorie
 */
class Categorie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Categorie
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    

    public function __toString(){
        return $this->getName();
    }

    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $conferences;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->conferences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add conference
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conference
     *
     * @return Categorie
     */
    public function addConference(\WA\CodenconfBundle\Entity\Conference $conference)
    {
        $this->conferences[] = $conference;

        return $this;
    }

    /**
     * Remove conference
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conference
     */
    public function removeConference(\WA\CodenconfBundle\Entity\Conference $conference)
    {
        $this->conferences->removeElement($conference);
    }

    /**
     * Get conferences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConferences()
    {
        return $this->conferences;
    }
}
