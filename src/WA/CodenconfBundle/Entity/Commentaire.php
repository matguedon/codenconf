<?php

namespace WA\CodenconfBundle\Entity;

/**
 * Commentaire
 */
class Commentaire
{
    /**
     * @var integer
     */
    private $id;
    
    /**
     * @var string
     */
    private $texte;
    
    /**
     * @var \WA\CodenconfBundle\Entity\Conference
     */
    private $conference;

    /**
     * @var \WA\UtilisateurBundle\Entity\Utilisateur
     */
    private $utilisateur;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set texte
     *
     * @param string $texte
     *
     * @return Commentaire
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get texte
     *
     * @return string
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Set conference
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conference
     *
     * @return Commentaire
     */
    public function setConference(\WA\CodenconfBundle\Entity\Conference $conference = null)
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * Get conference
     *
     * @return \WA\CodenconfBundle\Entity\Conference
     */
    public function getConference()
    {
        return $this->conference;
    }


    /**
     * Set utilisateur
     *
     * @param \WA\UtilisateurBundle\Entity\Utilisateur $utilisateur
     *
     * @return Commentaire
     */
    public function setUtilisateur(\WA\UtilisateurBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \WA\CodenconfBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Get utilisateur username
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->utilisateur;
    }


    /**
     * @var \DateTime
     */
    private $date;


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Commentaire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
