<?php

namespace WA\CodenconfBundle\Entity;

/**
 * Conference
 */
class Conference
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commentaires;
    
    /**
     * @var string
     */
    private $titre;

    /**
     * @var string
     */
    private $lieu;

    /**
     * @var string
     */
    private $description;
    
    /**
     * @var \DateTime
     */
    private $dateDebut;
    
    /**
     * @var \DateTime
     */
    private $dateFin;

    /**
     * @var boolean
     */
    private $annulee;
    
    /**
     * @var \WA\UtilisateurBundle\Entity\Utilisateur
     */
    private $createur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $notes;

        /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $abonnes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $videos;

        /**
     * @var \WA\CodenconfBundle\Entity\Langue
     */
    private $langues;
     
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categories;

    /**
     * @var string
     */
    private $site;

    private $imagePresentation;

    private $validee;

    
    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->commentaires = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Conference
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add commentaire
     *
     * @param \WA\CodenconfBundle\Entity\Commentaire $commentaire
     *
     * @return Conference
     */
    public function addCommentaire(\WA\CodenconfBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    /**
     * Remove commentaire
     *
     * @param \WA\CodenconfBundle\Entity\Commentaire $commentaire
     */
    public function removeCommentaire(\WA\CodenconfBundle\Entity\Commentaire $commentaire)
    {
        $this->commentaires->removeElement($commentaire);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }
   
    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Conference
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }




    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return Conference
     */
    public function setLieu($lieu)
    {
        $this->lieu= $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Conference
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Conference
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set annulee
     *
     * @param boolean $annulee
     *
     * @return Conference
     */
    public function setAnnulee($annulee)
    {
        $this->annulee = $annulee;

        return $this;
    }

    /**
     * Get annulee
     *
     * @return boolean
     */
    public function getAnnulee()
    {
        return $this->annulee;
    }

    /**
     * Add note
     *
     * @param \WA\CodenconfBundle\Entity\Note $note
     *
     * @return Conference
     */
    public function addNote(\WA\CodenconfBundle\Entity\Note $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \WA\CodenconfBundle\Entity\Note $note
     */
    public function removeNote(\WA\CodenconfBundle\Entity\Note $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Get count notes
     *
     * @return int
     */
    public function getCountNotes()
    {
        return count($this->notes);
    }

    /**
     * Get count notes inscrits
     *
     * @return int
     */
    public function getCountNotesInscrits()
    {
        $somme = 0;
        if (!$this->getCountNotes()){
            return 0;
        }        
        foreach ($this->notes as $n){
            if ($n->getUtilisateur()) {
                $somme++;
            }
        }
        return $somme;
    }

    /**
     * Get average notes
     *
     * @return double
     */
    public function getAverageNotes()
    {
        $somme = 0;
        if (!$this->getCountNotes()){
            return 0;
        }        
        foreach ($this->notes as $n){
            $somme += $n->getValeur();
        }
        return round($somme/$this->getCountNotes(), 2);
    }

    /**
     * Get rounded average notes
     *
     * @return int
     */
    public function getRoundAverageNotes()
    {
        return round($this->getAverageNotes());
    }

    /**
     * Get array notes
     *
     * @return array
     */
    public function getArrayNotes()
    {
        $nbNotes= $this->getCountNotes();
        $array = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0
        );
        if (!$this->getCountNotes()){
            return $array;
        }  
        foreach ($this->notes as $n){
            $array[$n->getValeur()]++;
        }
        foreach ($array as $i => $value) {
            $array[$i] = $value/$nbNotes*100;
        }
        return $array;
    }

    /**
     * Get percent notes
     *
     * @return double
     */
    public function getPercentNotes($i)
    {
        return round($this->getArrayNotes()[$i]);
    }



    /**
     * Set createur
     *
     * @param \WA\UtilisateurBundle\Entity\Utilisateur $createur
     *
     * @return Conference
     */
    public function setCreateur(\WA\UtilisateurBundle\Entity\Utilisateur $createur = null)
    {
        $this->createur = $createur;

        return $this;
    }

    /**
     * Get createur
     *
     * @return \WA\UtilisateurBundle\Entity\Utilisateur
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * Set abonne
     *
     * @param \WA\UtilisateurBundle\Entity\Utilisateur $abonne
     *
     * @return Conference
     */
    public function setAbonne(\WA\UtilisateurBundle\Entity\Utilisateur $abonne = null)
    {
        $this->abonne = $abonne;

        return $this;
    }

    /**
     * Get abonne
     *
     * @return \WA\UtilisateurBundle\Entity\Utilisateur
     */
    public function getAbonne()
    {
        return $this->abonne;
    }


    /**
     * Add abonne
     *
     * @param \WA\UtilisateurBundle\Entity\Utilisateur $abonne
     *
     * @return Conference
     */
    public function addAbonne(\WA\UtilisateurBundle\Entity\Utilisateur $abonne)
    {
        $this->abonnes[] = $abonne;

        return $this;
    }

    /**
     * Remove abonne
     *
     * @param \WA\UtilisateurBundle\Entity\Utilisateur $abonne
     */
    public function removeAbonne(\WA\UtilisateurBundle\Entity\Utilisateur $abonne)
    {
        $this->abonnes->removeElement($abonne);
    }

    /**
     * Get abonnes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbonnes()
    {
        return $this->abonnes;
    }

    /**
     * Set langues
     *
     * @param \WA\CodenconfBundle\Entity\Langue $langues
     *
     * @return Conference
     */
    public function setLangues(\WA\CodenconfBundle\Entity\Langue $langues = null)
    {
        $this->langues = $langues;

        return $this;
    }

    /**
     * Get langues
     *
     * @return \WA\CodenconfBundle\Entity\Langue
     */
    public function getLangues()
    {
        return $this->langues;
    }
   

    /**
     * Add category
     *
     * @param \WA\CodenconfBundle\Entity\Categorie $category
     *
     * @return Conference
     */
    public function addCategory(\WA\CodenconfBundle\Entity\Categorie $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \WA\CodenconfBundle\Entity\Categorie $category
     */
    public function removeCategory(\WA\CodenconfBundle\Entity\Categorie $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add langue
     *
     * @param \WA\CodenconfBundle\Entity\Langue $langue
     *
     * @return Conference
     */
    public function addLangue(\WA\CodenconfBundle\Entity\Langue $langue)
    {
        $this->langues[] = $langue;

        return $this;
    }

    /**
     * Remove langue
     *
     * @param \WA\CodenconfBundle\Entity\Langue $langue
     */
    public function removeLangue(\WA\CodenconfBundle\Entity\Langue $langue)
    {
        $this->langues->removeElement($langue);
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return Conference
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set imagePresentation
     *
     * @param \WA\CodenconfBundle\Entity\Image $imagePresentation
     *
     * @return Conference
     */
    public function setImagePresentation(\WA\CodenconfBundle\Entity\Image $imagePresentation = null)
    {
        $this->imagePresentation = $imagePresentation;

        return $this;
    }

    /**
     * Get imagePresentation
     *
     * @return \WA\CodenconfBundle\Entity\Image
     */
    public function getImagePresentation()
    {
        return $this->imagePresentation;
    }
    /**
     * @var string
     */
    private $adresse;

    /**
     * @var string
     */
    private $codePostal;

    /**
     * @var string
     */
    private $ville;


    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Conference
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Conference
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Conference
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }
    /**
     * @var integer
     */
    private $tarifMin;

    /**
     * @var integer
     */
    private $tarifMax;


    /**
     * Set tarifMin
     *
     * @param integer $tarifMin
     *
     * @return Conference
     */
    public function setTarifMin($tarifMin)
    {
        $this->tarifMin = $tarifMin;

        return $this;
    }

    /**
     * Get tarifMin
     *
     * @return integer
     */
    public function getTarifMin()
    {
        return $this->tarifMin;
    }

    /**
     * Set tarifMax
     *
     * @param integer $tarifMax
     *
     * @return Conference
     */
    public function setTarifMax($tarifMax)
    {
        $this->tarifMax = $tarifMax;

        return $this;
    }

    /**
     * Get tarifMax
     *
     * @return integer
     */
    public function getTarifMax()
    {
        return $this->tarifMax;
    }
    /**
     * @var string
     */
    private $descriptionCourte;

    /**
     * @var string
     */
    private $descriptionLongue;


    /**
     * Set descriptionCourte
     *
     * @param string $descriptionCourte
     *
     * @return Conference
     */
    public function setDescriptionCourte($descriptionCourte)
    {
        $this->descriptionCourte = $descriptionCourte;

        return $this;
    }

    /**
     * Get descriptionCourte
     *
     * @return string
     */
    public function getDescriptionCourte()
    {
        return $this->descriptionCourte;
    }

    /**
     * Set descriptionLongue
     *
     * @param string $descriptionLongue
     *
     * @return Conference
     */
    public function setDescriptionLongue($descriptionLongue)
    {
        $this->descriptionLongue = $descriptionLongue;

        return $this;
    }

    /**
     * Get descriptionLongue
     *
     * @return string
     */
    public function getDescriptionLongue()
    {
        return $this->descriptionLongue;
    }

    /**
     * Set validee
     *
     * @param boolean $validee
     *
     * @return Conference
     */
    public function setValidee($validee)
    {
        $this->validee = $validee;

        return $this;
    }

    /**
     * Get validee
     *
     * @return boolean
     */
    public function getValidee()
    {
        return $this->validee;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Conference
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Conference
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }
}
