<?php

namespace WA\CodenconfBundle\Entity;


/**
 * Langue
 */
class Langue
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    private $conferences;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Langue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

   

    public function __toString(){
        return $this->getName();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->conferences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add conference
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conference
     *
     * @return Langue
     */
    public function addConference(\WA\CodenconfBundle\Entity\Conference $conference)
    {
        $this->conferences[] = $conference;

        return $this;
    }

    /**
     * Remove conference
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conference
     */
    public function removeConference(\WA\CodenconfBundle\Entity\Conference $conference)
    {
        $this->conferences->removeElement($conference);
    }

    /**
     * Get conferences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConferences()
    {
        return $this->conferences;
    }
}
