<?php

namespace WA\CodenconfBundle\Entity;

/**
 * Note
 */
class Note
{
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var integer
     */
    private $valeur;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \WA\CodenconfBundle\Entity\Conference
     */
    private $conference;

    /**
     * @var \WA\UtilisateurBundle\Entity\Utilisateur
     */
    private $utilisateur;


    /**
     * Set valeur
     *
     * @param integer $valeur
     *
     * @return Note
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get valeur
     *
     * @return integer
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Note
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set conference
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conference
     *
     * @return Note
     */
    public function setConference(\WA\CodenconfBundle\Entity\Conference $conference = null)
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * Get conference
     *
     * @return \WA\CodenconfBundle\Entity\Conference
     */
    public function getConference()
    {
        return $this->conference;
    }

    /**
     * Set utilisateur
     *
     * @param \WA\UtilisateurBundle\Entity\Utilisateur $utilisateur
     *
     * @return Note
     */
    public function setUtilisateur(\WA\UtilisateurBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \WA\UtilisateurBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
}
