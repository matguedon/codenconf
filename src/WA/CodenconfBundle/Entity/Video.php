<?php

namespace WA\CodenconfBundle\Entity;

/**
 * Video
 */
class Video
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nameVideo;

    /**
     * @var string
     */
    private $url;
    
    /**
     * @var \WA\CodenconfBundle\Entity\Conference
     */
    private $conference;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameVideo
     *
     * @param string $nameVideo
     *
     * @return Video
     */
    public function setNameVideo($nameVideo)
    {
        $this->nameVideo = $nameVideo;

        return $this;
    }

    /**
     * Get nameVideo
     *
     * @return string
     */
    public function getNameVideo()
    {
        return $this->nameVideo;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set conference
     *
     * @param \WA\CodenconfBundle\Entity\Conference $conference
     *
     * @return Video
     */
    public function setConference(\WA\CodenconfBundle\Entity\Conference $conference = null)
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * Get conference
     *
     * @return \WA\CodenconfBundle\Entity\Conference
     */
    public function getConference()
    {
        return $this->conference;
    }
}
