<?php

namespace WA\CodenconfBundle\Services;

use Doctrine\ORM\EntityManager;

class MoteurDeRecherche{
	private $em;

	public function __construct(EntityManager $entityManager)
	{
		$this->em = $entityManager;
	}

	public function rechercheParVille($ville){
		return $this->em->getRepository('WACodenconfBundle:Conference')
		->findByVille($ville);
	}

	public function rechercheParCategories($categorie){
		return $this->em->getRepository('WACodenconfBundle:Conference')
		->findByCategorie($categorie);
	}

	public function recherche($motCle, $categories, $ville){
		$listConferences = null;

		if($motCle != "all" && $motCle != "top"){
			$listConferences = $this->em->getRepository('WACodenconfBundle:Conference')
			->findByMotCle($motCle);
		}
		else{
			$listConferences = $this->em->getRepository('WACodenconfBundle:Conference')
			->findAll();
		}

		if($categories != null && count($categories) != 0){
			//Récupération des conférences liées à la catégorie courante
			$conferences = $this->em->getRepository('WACodenconfBundle:Conference')
			->findByCategories($categories);

			//Adaptation de la liste principale
			$conferencesTmp = $listConferences;
			foreach ($conferencesTmp as $conference) {
					if (!in_array($conference, $conferences))
						unset($listConferences[array_search($conference, $conferencesTmp)]);
			}
		}

		if($ville != null && $ville != ""){
			//Récupération des conférences dans la ville
			$conferences = $this->em->getRepository('WACodenconfBundle:Conference')
			->findByVille($ville);

			//Adaptation de la liste principale
			$conferencesTmp = $listConferences;
			foreach ($conferencesTmp as $conference) {
				if($conference->getVille() != $ville){
					unset($listConferences[array_search($conference, $conferencesTmp)]);
				}
			}
		}

		return $listConferences;
	}
}