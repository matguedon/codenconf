<?php

namespace WA\CodenconfBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WA\CodenconfBundle\Entity\Conference;
use WA\CodenconfBundle\Entity\Image;
use WA\CodenconfBundle\Entity\Note;
use WA\CodenconfBundle\Entity\Commentaire;
use WA\CodenconfBundle\Form\SaisieConferenceType;
use WA\CodenconfBundle\Form\NewImageType;
use WA\CodenconfBundle\Form\NoteType;
use WA\CodenconfBundle\Form\CommentaireType;
use WA\CodenconfBundle\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Google\Service\Calendar;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    public function rechercheAction(Request $request){
        $em = $this->get('doctrine')->getManager();
        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        $utilisateurCourant = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->get('doctrine')->getManager();
        $utilisateur = $em->getRepository('WAUtilisateurBundle:Utilisateur')
        ->findById($utilisateurCourant);


        return $this->render('include/header.html.twig', array(
            'categories' => $categories,
            'utilisateur' => $utilisateur

            ));
    }

    public function indexAction(Request $request)
    {
    	$currentRoute = $this->container->get('request')->get('_route');
    	$currentURL = $this->get('router')->generate($currentRoute, array(), true);

        $em = $this->get('doctrine')->getManager();
        $conference = $em->getRepository('WACodenconfBundle:Conference')
        ->findById(1);


        return $this->render('WACodenconfBundle:Default:index.html.twig', array(
            'base_url' => $currentURL,
            'conference' => $conference[0]
            ));

    }

    public function conferencesAction($motCle)
    {
        $em = $this->get('doctrine')->getManager();

        $parametres = explode('&', $motCle);
        $motCle = $parametres[0];
        
        // Récupération des filtres
        $categories = null;
        $ville = null;

        if(isset($parametres[1]) && explode("=", $parametres[1])[0] == "categories")
            $categories = explode("=", $parametres[1])[1];
        else if(isset($parametres[2]) && explode("=", $parametres[2])[0] == "categories")
            $categories = explode("=", $parametres[2])[1];


        if(isset($parametres[1]) && explode("=", $parametres[1])[0] == "ville")
            $ville = explode("=", $parametres[1])[1];
        else if(isset($parametres[2]) && explode("=", $parametres[2])[0] == "ville")
            $ville = explode("=", $parametres[2])[1];

        if ($categories != null)
            $categories = explode(",", $categories);

        // Récupération de la liste des conférences correspondantes
        $moteurDeRecherche = $this->container->get('wa_codenconf.moteur_de_recherche');
        $conferences = $moteurDeRecherche->recherche($motCle, $categories, $ville);

        //Breadcrumb
        $breadcrumb = array();
        array_push($breadcrumb, "Recherche");
        array_push($breadcrumb, $motCle);

        return $this->render('WACodenconfBundle:Default:conferences.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'mot_cle' => $motCle,
            'conferences' => $conferences
            ));
    }

    public function conferencesProximiteAction()
    {
        $ville="bordeaux";
        $em = $this->get('doctrine')->getManager();
        $conferences = $em->getRepository('WACodenconfBundle:Conference')
        ->findByVille($ville);

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        $breadcrumb = array();
        array_push($breadcrumb, "Conférences");

        return $this->render('WACodenconfBundle:Default:conferences.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'mot_cle' => $ville,
            'conferences' => $conferences,
            'categories' => $categories
            ));
    }

    public function saisieConferenceAction(Request $request){
        $em = $this->get('doctrine')->getManager();
        $conference = new Conference();
        $form = $this->createForm(new SaisieConferenceType(), $conference);

        if ($form->handleRequest($request)->isValid()) {
            $conference->setAnnulee(false);
            $conference->setValidee(false);
    		//$conference->setCreateur($utilisateurCourant);

            $imagePresentation = $conference->getImagePresentation();
            $titre = $conference->getTitre();
            $titre = str_replace(" ", "-", $titre);
            $imagePresentation->setName($titre);
            $imagePresentation->upload($titre);

            $em->persist($conference);
            $em->flush();

    	       //	$request->getSession()->getFlashBag()->add('notice', 'Conférence bien enregistrée.');

            return $this->redirectToRoute('wa_codenconf_accueil');
        }

        $breadcrumb = array();
        array_push($breadcrumb, "Conférences");
        array_push($breadcrumb, "Nouvelle conférence");


        return $this->render('WACodenconfBundle:Default:saisieConference.html.twig', array(
            'form' => $form->createView(),
            'breadcrumb' => $breadcrumb,
            ));


    }

    public function conferenceAction($conferenceName, Request $request)
    {
        $conferenceName = str_replace("-", " ", $conferenceName);

        $em = $this->get('doctrine')->getManager();
        $conference = $em->getRepository('WACodenconfBundle:Conference')
        ->findByTitre($conferenceName);

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        $ip = $this->container->get('request')->getClientIp();
        $user = $this->get('security.context')->getToken()->getUser();
        if ($user == "anon."){$user = null;}
        
        if ($user){
            $note = $em->getRepository('WACodenconfBundle:Note')
            ->findBy(array('conference' => $conference[0]->getId(), 'utilisateur' => $user->getId()));
        }
        else{
            $note = $em->getRepository('WACodenconfBundle:Note')
            ->findBy(array('conference' => $conference[0]->getId(), 'description' => $ip));
        }
        if($note){
            $note = $note[0];
        }else{
            $note = new Note();
        }

        $formNote = $this->createForm(new NoteType(), $note);
        if ($formNote->handleRequest($request)->isValid()) {

            $note = $formNote->getData();
            $note->setDescription($ip);
            $note->setConference($conference[0]);
            $note->setUtilisateur($user);
            $em->persist($note);
            $em->flush();
        }

        $com = new Commentaire();
        $formCom = $this->createForm(new CommentaireType(), $com);
        if ($formCom->handleRequest($request)->isValid()) {
            $com = $formCom->getData();
            $com->setConference($conference[0]);
            $com->setUtilisateur($user);
            $com->setDate(new \DateTime());
            $em->persist($com);
            $em->flush();
        }

        $breadcrumb = array();
        array_push($breadcrumb, "Conférences");
        array_push($breadcrumb, $conferenceName);

        $url_full = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $url = $_SERVER['PHP_SELF'];

        return $this->render('WACodenconfBundle:Default:conference.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'conference' => $conference[0],
            'url_full' => $url_full,
            'url' => $url,
            'categories' => $categories,
            'formNote' => $formNote->createView(),
            'formCom' => $formCom->createView()
            ));
    }

    public function agendaAction()
    {
        $em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "Agenda");

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        return $this->render('WACodenconfBundle:Default:agenda.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'categories' => $categories
            ));
    }

    public function inscriptionAction()
    {
        $em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "Inscription");

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        return $this->render('WACodenconfBundle:Default:inscription.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'categories' => $categories
            ));

    }

    public function newsletterAction()
    {
        $em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "Newsletter");

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        return $this->render('WACodenconfBundle:Default:newsletter.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'categories' => $categories
            ));
    }

    public function aboutAction(){
        $em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "A propos");

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        return $this->render('WACodenconfBundle:Default:about.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'categories' => $categories
            ));
    }

    public function contactAction(Request $request)
    {
        //$em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "Contact");

        $form = $this->createForm(new ContactType());

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                ->setSubject($form->get('subject')->getData())
                ->setFrom($form->get('email')->getData())
                ->setTo('webagora.contact@gmail.com')
                ->setBody(
                    $this->renderView(
                        'email/messageContact.html.twig',
                        array(
                            'name' => $form->get('name')->getData(),
                            'message' => $form->get('message')->getData(),
                            'email' => $form->get('email')->getData()
                            )
                        ),
                    'text/html'
                    );
                $this->get('mailer')->send($message);

                $request->getSession()->getFlashBag()->add('success', 'Votre message a été envoyé');
            }
        }

        return $this->render('WACodenconfBundle:Default:contact.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'form' => $form->createView()
            ));
    }

    public function mentionsLegalesAction()
    {
        $em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "Mentions légales");

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        return $this->render('WACodenconfBundle:Default:mentionsLegales.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'categories' => $categories));

    }

    public function acteursAction()
    {
        $em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "Acteurs");

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        return $this->render('WACodenconfBundle:Default:acteurs.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'categories' => $categories
            ));
    }

    public function planSiteAction()
    {
        $em = $this->get('doctrine')->getManager();

        $breadcrumb = array();
        array_push($breadcrumb, "Plan du site");

        $categories = $em->getRepository('WACodenconfBundle:Categorie')
        ->findAll();

        return $this->render('WACodenconfBundle:Default:planSite.html.twig', array(
            'breadcrumb' => $breadcrumb,
            'categories' => $categories));

    }

    public function getSuggestionsAction(){
        $request = $this->container->get('request');        
        $recherche = $_REQUEST['recherche'];
        $categories = array();
        if(isset($_REQUEST['categories']))
            $categories = $_REQUEST['categories'];
        $ville = $_REQUEST['ville'];

        $em = $this->get('doctrine')->getManager();

        $moteurDeRecherche = $this->container->get('wa_codenconf.moteur_de_recherche');
        $conferences = $moteurDeRecherche->recherche($recherche, $categories, $ville);

        $conference0 = $conferences[0];
        $conference1 = $conferences[1];
        $conference2 = $conferences[2];

        $suggestions = array();
        $max = 5;
        if(count($conferences) < 5)
            $max = count($conferences);
        for($i=0; $i<$max; $i++){
            array_push($suggestions,$conferences[$i]->getTitre());
        }

        $response = array("code" => 100, "success" => true, "data" => $suggestions);

        return new Response(json_encode($response)); 
    }
}
