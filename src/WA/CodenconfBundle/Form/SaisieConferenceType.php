<?php

namespace WA\CodenconfBundle\Form;

use WA\CodenconfBundle\Form\ConferenceType;
use WA\CodenconfBundle\Form\NewImageType;
use Symfony\Component\Form\FormBuilderInterface;

class SaisieConferenceType extends ConferenceType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	parent::buildForm($builder, $options);
        $builder
            ->remove('annulee')
            ->remove('createur')
            ->remove('abonnes')
            ->remove('validee')
            ->add('imagePresentation', new NewImageType(), array(
                'required' => false
                ))
            ->add('categories', 'entity', array(
                'class' => 'WACodenconfBundle:Categorie',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'label' => 'Catégories : '
                ))
            ->add('langues', 'entity', array(
                'class' => 'WACodenconfBundle:Langue',
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'label' => 'Langues : '
                ))
            ->add('save', 'submit', array('label' => 'Enregistrer'))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wa_codenconfbundle_saisie_conference';
    }
}
