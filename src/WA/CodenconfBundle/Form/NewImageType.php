<?php

namespace WA\CodenconfBundle\Form;

use WA\CodenconfBundle\Form\ImageType;
use Symfony\Component\Form\FormBuilderInterface;

class NewImageType extends ImageType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	parent::buildForm($builder, $options);
        $builder
            ->add('file', 'file')
            ->remove('path')
            ->remove('name');
    }


    

    /**
     * @return string
     */
    public function getName()
    {
        return 'wa_codenconfbundle_new_image';
    }
}
