<?php

namespace WA\CodenconfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConferenceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('lieu')
            ->add('adresse')
            ->add('codePostal')
            ->add('ville')
            ->add('descriptionCourte')
            ->add('descriptionLongue')
            ->add('dateDebut')
            ->add('dateFin')
            ->add('annulee')
            ->add('site')
            ->add('tarifMin')
            ->add('tarifMax')
            ->add('validee')
            ->add('imagePresentation')
            ->add('createur')
            ->add('abonnes')
            ->add('categories')
            ->add('langues')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WA\CodenconfBundle\Entity\Conference'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wa_codenconfbundle_conference';
    }
}
