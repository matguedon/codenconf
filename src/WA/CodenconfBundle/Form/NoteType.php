<?php

namespace WA\CodenconfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class   NoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('valeur', 'star_rating', array(
        'choices' => array('1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5), 'expanded' => true, 'multiple' => false
        ))
            ->add('save', 'submit', array('label' => 'Noter'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WA\CodenconfBundle\Entity\Note'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wa_codenconfbundle_note';
    }
}
