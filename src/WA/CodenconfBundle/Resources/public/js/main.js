$( document ).ready(function() {


    // toastr options

    toastr.options = {
    	"closeButton": true,
    	"debug": false,
    	"newestOnTop": false,
    	"progressBar": false,
    	"positionClass": "toast-top-center",
    	"preventDuplicates": false,
    	"onclick": null,
    	"showDuration": "1000",
    	"hideDuration": "1000",
    	"timeOut": "5000",
    	"extendedTimeOut": "1000",
    	"showEasing": "swing",
    	"hideEasing": "linear",
    	"showMethod": "fadeIn",
    	"hideMethod": "fadeOut"
    }



	//Calendrier 
	$('#calendar').fullCalendar({
		editable: false, // Don't allow editing of events
		//handleWindowResize: true,
		weekends: true, // Hide weekends
		minTime: '07:30:00', // Start time for the calendar
		maxTime: '22:00:00', // End time for the calendar
		lang: 'fr',


		displayEventTime: true, // Display event time
		events: {
			googleCalendarApiKey: 'AIzaSyBWTVfbDEHexSI-oMp7lL3QT_Bb2gF00E4', 
			googleCalendarId: 'mjj0u0ckhjkrslbk8mj25ov2c8@group.calendar.google.com',	
		}
	});



	// Gestion des onglets de la page une conf

	// Etat de base des onglets au chargement de la page
	$(".rates").hide();
	$(".intervenants").hide();
	$(".informations").hide();

	$("#coms").addClass("active");
	$("#description").addClass("active");

	// Changement de l'état des onglets au clic

	$( "#rates" ).click(function() {
		$(".coms").hide();
		$(".rates").show();
		$("#coms").removeClass("active");
		$("#rates").addClass("active");

	})
	$( "#coms" ).click(function() {
		$(".coms").show();
		$(".rates").hide();
		$("#rates").removeClass("active");
		$("#coms").addClass("active");

	})

	$( "#description" ).click(function() {
		$(".informations").hide();
		$(".description").show();
		$("#description").addClass("active");
		$("#informations").removeClass("active");
	})
	
	$( "#informations" ).click(function() {
		$(".description").hide();
		$(".informations").show();
		$("#description").removeClass("active");
		$("#informations").addClass("active");
	})

	// Remonter en haut de la page

	$('.arrow_up').click(function(event) {
		event.preventDefault();	
		$('html, body').animate({scrollTop: 0}, 300);
	})


	// Effet like card 
	$(".fav_conf").click(function(){
		$(this).toggleClass('glyphicon-heart-empty glyphicon-heart');
	});
	

	// dropdown du menu qui se ferme au clique exterieur au canvas

	var targetBouton = $('button.navbar-toggle');
	var targetMenu = $('.navbar-collapse');	

	$('html').bind("click touchstart", function(event) {
		if(!$(event.target).closest(targetMenu).length && $('.collapse.in').length) {
			if(!$(event.target).closest(targetBouton).length) {
				targetBouton.trigger("click");
			};
		};
	});

	//Lance la recherche quand on fait "entrer"
	$('#barre_recherche').keypress(function (e) {
		if (e.which == 13) {
			$('#recherche_button').click();
			return false;
		}
	});

});

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.5&appId=974267379279143";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function suppression_compte(){
	var texte = "Cette action sera définitive. Etes vous sûr de vouloir supprimer votre compte ?";
	return confirm(texte);
}

function indisponible(){
	var texte = "Cette action est indisponible pour le moment.";
	// alert(texte);
	return toastr.info(texte);

}

/* Google maps : affichage des lieux des conférences */
function initMap() {
	// Create a map object and specify the DOM element for display.
	var divMap = document.getElementById("map");
	var map = new google.maps.Map(divMap, {
		center: {lat: 46.4840495, lng: 2.444495},
		scrollwheel: false,
		zoom: 6
	});

	var villes = $("span[class^='ville_']");
	villes.each(function(){
		var nomVille = $(this).html();
		var conferenceTitle = $(this).parent().siblings(".card_title").children("a[title^='Conférence']").attr("title");
		var titre = "";
		$.each(conferenceTitle.split(" "), function(index, element){
			if(index > 0)
				titre += element + " ";
		});
		setMarker(nomVille, map, titre);
	});
}

function setMarker(adresse, map, titre) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({ 'address': adresse }, function (results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			var latitude = results[0].geometry.location.lat();
			var longitude = results[0].geometry.location.lng();

			var coord = new google.maps.LatLng(latitude, longitude);
			var marker = new google.maps.Marker({
				map: map,
				position: coord,
				title: titre
			});

		}
	});
};

function ouvrir_filtres(){
	$('div#filtres').show();
	$('button.close_filter').show();
	$('button.open_filter').hide();
}

function fermer_filtres(){
	$('div#filtres').hide();
	$('button.close_filter').hide();
	$('button.open_filter').show();
}


