$(document).ready(function() {
	var div_suggestions = $("div.suggestions");
	div_suggestions.hide();

	$("#recherche_button").click(function(event){

		var getUrl = window.location;
		var url = getUrl.protocol + "//" + getUrl.host + $("#recherche_button").attr('name') + "/";

		var recherche = $("#barre_recherche").val();

		var categoriesChecked = new Array();
		var categoriesInput = $("#categorie input:checked");

		categoriesInput.each(function(){
			categoriesChecked.push($(this).attr('name').split("_")[1]);
		});

		//Récupération de la ville entrée
		var inputVille = $("input[name='champ_ville']");
		var ville = inputVille.val();

		if(recherche == "")
			url += "all";
		else
			url += recherche;

		//Ajout des catégories et de la ville à l'url
		if (ville != ""){
			url += "&ville=" + ville;
		}

		if (categoriesChecked.length > 0){
			url += "&categories=";
			url += categoriesChecked.join(",");
		}
		
		

		document.location.href = url;
	});

	$("#barre_recherche").keyup(function(event){
		if(event.keyCode == 8 && $("#barre_recherche").val().length <= 1){
			div_suggestions.empty();
			div_suggestions.hide();
		}
		else{
			var categories = new Array();
			$("#categorie input[name^=categorie_]:checked").each(function(){
				categories.push($(this).attr('name').split("_")[1]);
			});

			var inputVille = $("input[name='champ_ville']");
			var ville = inputVille.val();

			var getUrl = window.location;
			var url = getUrl.protocol + "//" + getUrl.host + $("#barre_recherche").attr('name');

			var promise = $.ajax({
				method: "POST",
				url: url,
				dataType: 'json',           
				data: {recherche: $(this).val(), categories: categories, ville: ville}
			});

			promise.done(function(response) {
				var suggestions = response.data;
				div_suggestions.fadeIn(300);

				div_suggestions.empty();

				var ul = $("<ul/>");
				$.each(suggestions, function(index, suggestion){
					var li_suggestion = $("<li/>").html(suggestion);
					li_suggestion.click(function(){
						$("#barre_recherche").val($(this).html());
						$("#recherche_button").click();
					});
					ul.append(li_suggestion);
				});
				div_suggestions.append(ul);
			});

			promise.fail(function(){
				div_suggestions.empty();
				div_suggestions.hide();
			});
		}
	});
});