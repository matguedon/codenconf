## Synopsis

Le site codenconf.fr est un site web permettant de recenser les conférences ayant lieu en France, concernant les sujets tels que: le web, les nouvelles technologies, la cybercriminalité, les reseaux sociaux, etc.
Il permet également aux utilisateurs enregistrés d'ajouter des conférences.

## Motivation

Ce projet est créé dans le cadre de la licence professionnelle Developpement d'Application Web et Image NumÃ©rique (DAWIN). Il répond cependant à un besoin, créer un annuaire de toutes les conférences centrées sur le numérique ayant lieu en France. Il y a en effet très peu de site proposant ce service.

## Contributors

Facebook:
https://www.facebook.com/codenconf/?fref=ts
Twitter:
https://twitter.com/CodenConf

## License

Cette oeuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 3.0 France.

## Team

L'équipe est composée de deux étudiantes : Mathilde Guedon et Juliette Riviere, ainsi que trois étudiants : Rémi Dong, Bassam Boulacheb et Maxence Pautrot
